package com.consultadd.service;

import com.consultadd.model.User;
import com.consultadd.model.UserDetailsGroup;
import com.consultadd.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(username);
        if(user==null) {
            throw new UsernameNotFoundException(username+" is not found");
        }

        UserDetails userDetails = new UserDetailsGroup(user);
        System.out.println(userDetails.getPassword());
        return userDetails;
    }

}
