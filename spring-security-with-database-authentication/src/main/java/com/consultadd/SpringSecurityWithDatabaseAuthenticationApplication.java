package com.consultadd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityWithDatabaseAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityWithDatabaseAuthenticationApplication.class, args);
	}

}
