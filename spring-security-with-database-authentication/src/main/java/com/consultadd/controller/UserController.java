package com.consultadd.controller;

import com.consultadd.model.User;
import com.consultadd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/hello")
    public String checkAPI() {
        return "Hello Admin User";
    }

    @PostMapping
    public String addUser(@RequestBody User user) {
        String encodedPwd = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPwd);
        userService.addUser(user);
        return "Successfully added";
    }








}
