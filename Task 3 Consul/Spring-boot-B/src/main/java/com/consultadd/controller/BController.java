package com.consultadd.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/b")
@EnableDiscoveryClient
public class BController {

    @Autowired
    private RestTemplate template;

    @Autowired
    private DiscoveryClient client;

    @GetMapping
    public String accessA() {
        URI uri = client.getInstances("application-A").stream().map(si-> si.getUri()).findFirst()
                .map(s->s.resolve("/a/greeting")).get();
        return template.getForObject(uri, String.class);
    }




}
